package com.impaq.hadoop.churn;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;


public class ContactsCounterMapper extends Mapper<Text, Text, Text, IntWritable> {

    private final IntWritable one = new IntWritable(1);

    @Override
    protected void map(Text key, Text value,
                       Context context)
            throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString());
        while (itr.hasMoreTokens()) {
            String token = itr.nextToken();
            context.write(new Text(token), one);
        }

    }
}
