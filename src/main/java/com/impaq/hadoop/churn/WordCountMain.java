package com.impaq.hadoop.churn;


import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;
import java.net.URI;

public class WordCountMain {

    public WordCountMain() {
        System.out.println("Init WordCountMain");
    }

    public static void main(String[] args) throws Exception{
        Configuration conf = new Configuration();

        conf.set("mapreduce.input.keyvaluelinerecordreader.key.value.separator", ",");

        String[] otherArgs = new GenericOptionsParser(conf,args).getRemainingArgs();

        // Pierwszy job liczy wystapienia danej osoby w kontaktach innych osob
        Job job1 = createJob(conf, ContactsCounterMapper.class, "churn/contacts.txt", "churn/temp");

        // Drugi Szuka slow ze slowniki we wejsciowym tekscie
        Job job2 = createJob(conf, WordMatcherMapper.class, "churn/input.txt", "churn/output");

        job1.waitForCompletion(true);

        //TODO W tej chwili to nie dziala odbieranie plikow z cachu
        URI uri = new URI("churn/temp/part-r-00000");
        DistributedCache.addCacheFile(uri, conf);

//        job2.setCacheFiles(new URI[]{uri});

        job2.waitForCompletion(true);

        System.exit(0);

    }

    private static Job createJob(Configuration conf, Class mapperClass, String input, String output) throws IOException {
        Job job = new Job(conf);
        job.setInputFormatClass(KeyValueTextInputFormat.class);

        job.setCombinerClass(GenericAdderReducer.class);
        job.setReducerClass(GenericAdderReducer.class);
        job.setMapperClass(mapperClass);
        job.setJarByClass(WordCountMain.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path(input));
        FileOutputFormat.setOutputPath(job, new Path(output));
        return job;
    }
}