package com.impaq.hadoop.churn;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;


public class WordMatcherMapper extends Mapper<Text, Text, Text, IntWritable> {
    private Set<String> dictionary = new HashSet<String>();

    private final IntWritable one = new IntWritable(1);

    public WordMatcherMapper() {

        dictionary.add("slowo");
        dictionary.add("wyraz");
    }

    @Override
    protected void map(Text key, Text value,
                       Context context)
            throws IOException, InterruptedException {

        StringTokenizer itr = new StringTokenizer(value.toString());
        while (itr.hasMoreTokens()) {
            String token = itr.nextToken();

            if (dictionary.contains(token)) {
                context.write(key, one);
            }
        }

    }

    /*
    TODO To nie dziala w tej chwili.
     */
    private void fetchDataFromDistributedCache(Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            Path pt = new Path(context.getCacheFiles()[0]);
            FileSystem fs = FileSystem.get(new Configuration());
            BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));

            for (String line = br.readLine(); line != null; line = br.readLine()) {
                line = br.readLine();
                stringBuilder.append(line);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }

}