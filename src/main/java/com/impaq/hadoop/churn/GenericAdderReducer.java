package com.impaq.hadoop.churn;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


public class GenericAdderReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

    private IntWritable result = new IntWritable();

    @Override
    protected void reduce(Text word, Iterable<IntWritable> intOne,
                          Context context)
            throws IOException, InterruptedException {

        int sum = 0;

        for (IntWritable anIntOne : intOne) {
            sum += anIntOne.get();
        }

        result.set(sum);

        context.write(word, result);
    }

}