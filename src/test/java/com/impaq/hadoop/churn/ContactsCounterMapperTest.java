package com.impaq.hadoop.churn;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;


public class ContactsCounterMapperTest {

    MapDriver<Text, Text, Text, IntWritable> mapDriver;

    @Before
    public void setUp() {
        ContactsCounterMapper mapper = new ContactsCounterMapper();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(new Text("stefan"), new Text("roman roman roman gienek"));
        mapDriver.withOutput(new Text("roman"), new IntWritable(1));
        mapDriver.withOutput(new Text("roman"), new IntWritable(1));
        mapDriver.withOutput(new Text("roman"), new IntWritable(1));
        mapDriver.withOutput(new Text("gienek"), new IntWritable(1));
        mapDriver.runTest();
    }
}
