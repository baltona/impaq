package com.impaq.hadoop.churn;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class GenericAdderReducerTest {

    public static final IntWritable ONE = new IntWritable(1);
    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;

    @Before
    public void setUp() {
        GenericAdderReducer reducer = new GenericAdderReducer();
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testMapper() throws IOException {
        List<IntWritable> occurences = new ArrayList<IntWritable>();

        IntWritable i = new IntWritable(7);
        for (int j = i.get(); j > 0; --j) {
            occurences.add(ONE);
        }

        Text text = new Text("text");
        reduceDriver.withInput(text, occurences);
        reduceDriver.withOutput(text, i);

        reduceDriver.runTest();
    }
}
