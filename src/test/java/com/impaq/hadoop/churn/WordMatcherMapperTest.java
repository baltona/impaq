package com.impaq.hadoop.churn;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class WordMatcherMapperTest {

    MapDriver<Text, Text, Text, IntWritable> mapDriver;
    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
    MapReduceDriver<Text, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;

    @Before
    public void setUp() {
        WordMatcherMapper mapper = new WordMatcherMapper();
        GenericAdderReducer reducer = new GenericAdderReducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver.withInput(new Text("stefan"), new Text("wyraz leksem wyraz leksem"));
        mapDriver.withOutput(new Text("stefan"), new IntWritable(1));
        mapDriver.withOutput(new Text("stefan"), new IntWritable(1));
        mapDriver.runTest();
    }
}
